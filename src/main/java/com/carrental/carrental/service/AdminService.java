package com.carrental.carrental.service;

import com.carrental.carrental.domain.Color;
import com.carrental.carrental.domain.Models;
import com.carrental.carrental.domain.State;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Service;

@Service
@Getter
@Setter
public class AdminService {

    private State state;
    private Color color;
    private Models models;

    private Double price;

    public AdminService() {
    }

    public AdminService(State state, Color color, Models models) {
        this.state = state;
        this.color = color;
        this.models = models;
    }
}

