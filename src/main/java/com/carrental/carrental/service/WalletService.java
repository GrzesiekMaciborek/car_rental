package com.carrental.carrental.service;

import com.carrental.carrental.domain.CarBorrowing;
import com.carrental.carrental.domain.CarRepairing;
import com.carrental.carrental.domain.Wallet;
import com.carrental.carrental.persistance.WalletRepository;
import org.apache.commons.collections4.IteratorUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class WalletService {

    @Autowired
    private WalletRepository walletRepository;

    public void addMoney (Double money, CarBorrowing carBorrowing){

        Iterable <Wallet> allWallet = walletRepository.findAll();

        List<Wallet> wallets = IteratorUtils.toList(allWallet.iterator());

        List<LocalDateTime> walletsDate = new ArrayList<>();

        for (Wallet wallet: wallets) {
            walletsDate.add(wallet.getTransactionDate());
        }

        Collections.sort(walletsDate, Collections.reverseOrder());

        LocalDateTime time = walletsDate.get(0);

        Wallet wallet = walletRepository.findWalletByTransactionDate(time);
        Double newPrice = wallet.addMoney(money);

        Wallet newWallet = new Wallet();
        newWallet.setTransactionDate(LocalDateTime.now());
        newWallet.setCashInWallet(newPrice);
        newWallet.setCarBorrowing(carBorrowing);

        walletRepository.save(newWallet);


    }

    public void addMoney (Double money, CarRepairing carRepairing){

        Iterable <Wallet> allWallet = walletRepository.findAll();

        List<Wallet> wallets = IteratorUtils.toList(allWallet.iterator());

        List<LocalDateTime> walletsDate = new ArrayList<>();

        for (Wallet wallet: wallets) {
            walletsDate.add(wallet.getTransactionDate());
        }

        Collections.sort(walletsDate, Collections.reverseOrder());

        LocalDateTime time = walletsDate.get(0);

        Wallet wallet = walletRepository.findWalletByTransactionDate(time);
        Double newPrice = wallet.addMoney(money);

        Wallet newWallet = new Wallet();
        newWallet.setTransactionDate(LocalDateTime.now());
        newWallet.setCashInWallet(newPrice);
        newWallet.setCarRepairing(carRepairing);

        walletRepository.save(newWallet);
    }

}
