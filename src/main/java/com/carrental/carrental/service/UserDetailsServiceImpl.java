package com.carrental.carrental.service;

import com.carrental.carrental.domain.SecurityUserDetails;
import com.carrental.carrental.domain.User;
import com.carrental.carrental.persistance.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findUserByLogin(username);
        if (user != null) {
            SecurityUserDetails securityUserDetails = new SecurityUserDetails(user);
            return securityUserDetails;
        } else
            throw new UsernameNotFoundException("no such user!");
    }
}