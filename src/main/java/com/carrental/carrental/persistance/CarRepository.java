package com.carrental.carrental.persistance;

import com.carrental.carrental.domain.Car;
import com.carrental.carrental.domain.Color;
import com.carrental.carrental.domain.Models;
import com.carrental.carrental.domain.State;
import org.springframework.data.repository.CrudRepository;


import java.util.List;

public interface CarRepository extends CrudRepository<Car, Long> {

    Car readCarByIdCar (Long idCar);

    List<Car> findAll();
    List<Car> findByState(State state);
    List<Car> findByModel(Models model);
    List<Car> findByColor(Color color);
    List<Car> findByModelAndColorAndState(Models models, Color color, State state);
    List<Car> findByModelAndColor(Models models, Color color);
    List<Car> findByModelAndState(Models models, State state);
    List<Car> findByColorAndState(Color color, State state);

}