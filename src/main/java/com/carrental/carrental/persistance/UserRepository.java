package com.carrental.carrental.persistance;

import com.carrental.carrental.domain.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.List;

public interface UserRepository extends CrudRepository<User, Long>{
    List<User> findAll();
    User findUserByLogin(String login);
}
