package com.carrental.carrental.persistance;

import com.carrental.carrental.domain.Wallet;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDateTime;

public interface WalletRepository extends CrudRepository<Wallet, Long> {
    Wallet findWalletByTransactionDate(LocalDateTime time);

}
