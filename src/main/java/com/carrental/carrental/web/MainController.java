package com.carrental.carrental.web;

import com.carrental.carrental.persistance.UserRepository;
import com.carrental.carrental.service.InformationService;
import com.carrental.carrental.service.LoggedInUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private InformationService informationService;

    @GetMapping(value = "/")
    public String home() {
        return "home";
    }

    @GetMapping(value = "/usercheck")
    public String usercheck() {
        String webPage = "/login";

        String role = LoggedInUserService.getRole();

        if (role.equals("ROLE_USER")) {
            webPage = "redirect:client/clientstart";
        } else if (role.equals("ROLE_ADMIN")) {
            webPage = "redirect:admin/adminstart";
        }

        return webPage;
    }
}
