package com.carrental.carrental.web;

import com.carrental.carrental.domain.*;
import com.carrental.carrental.persistance.*;
import com.carrental.carrental.service.AdminService;
import com.carrental.carrental.service.InformationService;
import com.carrental.carrental.service.LoggedInUserService;
import com.carrental.carrental.service.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping(value = "admin")
public class AdminController {

    private final Double REPAIR_CAR = -50.0;

    @Autowired
    private AdminService adminService;

    @Autowired
    private CarRepository carRepository;

    @Autowired
    private WalletService walletService;

    @Autowired
    private UserCarRepository userCarRepository;

    @Autowired
    private CarBorrowingRepository carBorrowingRepository;

    @Autowired
    private CarRepairingRepository carRepairingRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private InformationService informationService;


    private List<Color> colorsList;

    private List<Models> modelsList;

    private List<State> statesList;

    @PostConstruct
    public void initializer() {
        colorsList = Arrays.asList(Color.values());
        modelsList = Arrays.asList(Models.values());
        statesList = Arrays.asList(State.values());
    }

    @GetMapping(value = "add")
    public String prepareFormToAddCar(Model model) {

        colorsList = Arrays.asList(Color.values());

        model.addAttribute("models", Arrays.asList(Models.values()));
        model.addAttribute("colors", colorsList);
        model.addAttribute("car", new Car());
        return "admin/add";
    }

    @GetMapping(value = "adminCar/{chosenCarId}")
    public String adminCarInformation(@PathVariable("chosenCarId") String carId, Model model) {

        informationService.setCarId(Long.valueOf(carId));

        Long currentCarId = informationService.getCarId();
        Car car = carRepository.findById(currentCarId).get();

        UserCar userCar = userCarRepository.findByCar(car);
        CarBorrowing carBorrowing = carBorrowingRepository.findByUserCar(userCar);

        if (userCar != null) {
            model.addAttribute("userFirstName", userCar.getUser().getFirstName());
            model.addAttribute("userLastName", userCar.getUser().getLastName());
            model.addAttribute("userEnterDate", carBorrowing.getDateEnter());
            model.addAttribute("userExitDate", carBorrowing.getDateExit());
            model.addAttribute("userTravelFrom", carBorrowing.getCityEnter());
            model.addAttribute("userTravelTo", carBorrowing.getCityExit());
            model.addAttribute("priceAll", carBorrowing.getPriceForAllDay());

//            System.out.println("Jest!");
        } else {
            model.addAttribute("userFirstName", "----");
            model.addAttribute("userLastName", "----");
            model.addAttribute("userEnterDate", "----");
            model.addAttribute("userExitDate", "----");
            model.addAttribute("userTravelFrom", "----");
            model.addAttribute("userTravelTo", "----");
            model.addAttribute("priceAll", "----");

        }

        // System.out.println("CarId is: " + car.getIdCar());

        model.addAttribute("models", car.getModel());
        model.addAttribute("colors", car.getColor());
        model.addAttribute("idCar", car.getIdCar());
        model.addAttribute("price", car.getPrice());
        model.addAttribute("state", car.getState());
        model.addAttribute("url", car.getUrlPhoto());
        model.addAttribute("myWallet", adminService.getPrice());

        return "admin/admincarinfo";
    }

    @GetMapping(value = "adminstart")
    public String adminStart(Model model) {
        String login = LoggedInUserService.getLogin();
        model.addAttribute("login", login);
        return "admin/adminstart";
    }

    @GetMapping(value = "car")
    public String adminCar(Model model) {
        Iterable<Car> cars = carRepository.findAll();
        getString(adminService, model, cars);
        return "admin/car";
    }

    @PostMapping(value = "users")
    public String adminUser() {
        return "admin/users";
    }

    @PostMapping(value = "repair")
    public String repairCar(Model model) {

        Long idCar = informationService.getCarId();
        Car car = carRepository.readCarByIdCar(idCar);

        if (car.getState() == State.BROKEN) {
            car.setState(State.FREE);
            carRepository.save(car);
        }

        String login = LoggedInUserService.getLogin();
        User user = userRepository.findUserByLogin(login);

        carRepository.save(car);

        Iterable<Car> cars = carRepository.findAll();

        getString(adminService, model, cars);

        CarRepairing carRepairing = new CarRepairing();
        carRepairing.setCar(car);
        carRepairing.setUser(user);
        carRepairing.setLocalDateTime(LocalDateTime.now());
        carRepairingRepository.save(carRepairing);

        walletService.addMoney(REPAIR_CAR, carRepairing);

        return "admin/car";
    }

    @GetMapping(value = "cash")
    public String adminCash() {
        return "admin/cash";
    }

    @PostMapping(value = "addcar")
    public String addCar(@ModelAttribute Car car) {
        car.setState(State.FREE);
        carRepository.save(car);
        return "admin/addcar";
    }

    @PostMapping(value = "adminCarFilter")
    public String adminCarFilter(@ModelAttribute AdminService adminService, Model model) {

        informationService.setModel(adminService.getModels());
        Models modelFilter = informationService.getModel();
        informationService.setColor(adminService.getColor());
        Color colorFilter = informationService.getColor();
        informationService.setState(adminService.getState());
        State stateFilter = informationService.getState(); //TODO refactoring?? 3 line to delete... maybe not put information to informationService...

        Iterable<Car> cars = getFilter(modelFilter, colorFilter, stateFilter);

        // System.out.println("Working!");

        getString(adminService, model, cars);

        return "admin/car";
    }


    // duplicate code
    private Model getString(@ModelAttribute AdminService adminService, Model model, Iterable<Car> cars) {

        model.addAttribute("cars", cars);
        model.addAttribute("models", modelsList);
        model.addAttribute("model", Models.values());
        model.addAttribute("colors", colorsList);
        model.addAttribute("color", Color.values());
        model.addAttribute("states", statesList);
        model.addAttribute("state", State.values());
        model.addAttribute("adminService", adminService);

        return model;
    }

    // filter of Cars
    private Iterable<Car> getFilter(Models modelFilter, Color colorFilter, State stateFilter) {

        Iterable<Car> cars = carRepository.findAll();

        if (modelFilter != Models.NONE) {
            if (colorFilter != Color.NONE) {
                if (stateFilter != State.NONE) {
                    cars = carRepository.findByModelAndColorAndState(modelFilter, colorFilter, stateFilter);
                } else {
                    cars = carRepository.findByModelAndColor(modelFilter, colorFilter);
                }
            } else if (stateFilter != State.NONE) {
                cars = carRepository.findByModelAndState(modelFilter, stateFilter);
            } else {
                cars = carRepository.findByModel(modelFilter);
            }

        } else if (colorFilter != Color.NONE) {
            if (stateFilter != State.NONE) {
                cars = carRepository.findByColorAndState(colorFilter, stateFilter);
            } else {
                cars = carRepository.findByColor(colorFilter);
            }
        } else if (stateFilter != State.NONE) {
            cars = carRepository.findByState(stateFilter);
        }
        return cars;
    }


}
