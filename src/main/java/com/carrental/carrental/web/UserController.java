package com.carrental.carrental.web;

import com.carrental.carrental.domain.*;
import com.carrental.carrental.persistance.*;
import com.carrental.carrental.service.InformationService;
import com.carrental.carrental.service.WalletService;
import com.carrental.carrental.service.LoggedInUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping(value = "client")

public class UserController {

    private final Double OTHER_CITY_EXTRA_PAYMENT = 100.0;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CarRepository carRepository;

    @Autowired
    private UserCarRepository userCarRepository;

    @Autowired
    private CarBorrowingRepository carBorrowingRepository;

    @Autowired
    private InformationService informationService;

    @Autowired
    private WalletService walletService;

    private List<City> citi;

    @PostConstruct
    public void initializer() {
        citi = Arrays.asList(City.values());
    }

    @GetMapping(value = "clientstart")
    public String clientStart(Model model) {
        String login = LoggedInUserService.getLogin();
        String role = LoggedInUserService.getRole();

        model.addAttribute("login", login);
        model.addAttribute("role", role);
        return "client/clientstart";
    }

    @GetMapping(value = "clientinfo")
    public String clientInfo(Model model) {
        String login = LoggedInUserService.getLogin();
        String role = LoggedInUserService.getRole();

        model.addAttribute("login", login);
        model.addAttribute("role", role);

        User user = userRepository.findUserByLogin(login);
        List<UserCar> userCarList = userCarRepository.findByUser(user);

        Double totalPrice = 0.0;
        List<CarBorrowing> carBorrowings = new ArrayList<>();
        for (UserCar userCar : userCarList) {
            CarBorrowing carBorrowing = carBorrowingRepository.findByUserCar(userCar);
            carBorrowings.add(carBorrowing);
            totalPrice += carBorrowing.getPriceForAllDay();
        }

        model.addAttribute("userCarList", userCarList);
        model.addAttribute("carBorrowings", carBorrowings);
        model.addAttribute("totalPrice", totalPrice);

        return "client/clientinfo";
    }

    @GetMapping(value = "carlist")
    public String cars(Model model) {
        Iterable<Car> cars = carRepository.findByState(State.FREE);
        model.addAttribute("cars", cars);
        return "client/carlist";
    }

    @GetMapping(value = "carlist/{chosenCarId}")
    public String rentCarByUser(@PathVariable("chosenCarId") String carId, Model model) {

        informationService.setCarId(Long.valueOf(carId));

        String login = LoggedInUserService.getLogin();
        User user = userRepository.findUserByLogin(login);

        Long chosenCarId = informationService.getCarId();
        Car car = carRepository.findById(chosenCarId).get();

        model.addAttribute("chosenCar", car);
        model.addAttribute("currentUser", user);

        return "client/confirmation";
    }

    @GetMapping(value = "borrowtime")
    public String countTime(Model model) {
        model.addAttribute("citi", citi);
        model.addAttribute("carborrowing", new CarBorrowing());
        return "client/borrowtime";
    }

    @PostMapping(value = "/time")
    public String time(@ModelAttribute CarBorrowing carBorrowing, Model model) { //add dateEnter, and dateExit

        informationService.setEnterDate(carBorrowing.getDateEnter());
        informationService.setExitDate(carBorrowing.getDateExit());

        informationService.setCityEnter(carBorrowing.getCityEnter());
        informationService.setCityExit(carBorrowing.getCityExit());

        Long enterDate = carBorrowing.getDateEnter().toEpochDay();
        Long exitDate = carBorrowing.getDateExit().toEpochDay();
        Long duringTimeInDays = exitDate - enterDate;

        String enterDateToString = carBorrowing.getDateEnter().toString();
        String exitDateToString = carBorrowing.getDateExit().toString();

        String login = LoggedInUserService.getLogin();
        User user = userRepository.findUserByLogin(login); // create User who want to drive

        model.addAttribute("user", user.getFirstName());
        model.addAttribute("lastName", user.getLastName());

        if (enterDate < LocalDate.now().toEpochDay()) { // we check, when is enterDate -  maybe before LocalDate.now()
            String nowDate = LocalDate.now().toString();

            model.addAttribute("nowDate", nowDate);
            model.addAttribute("enterDate", enterDateToString);
            return "client/wrongdatetonow";
        }

        if (duringTimeInDays <= 0) { // when User take Date "in minus" :)

            model.addAttribute("enterDate", enterDateToString);
            model.addAttribute("exitDate", exitDateToString);

            return "client/wrongdate";
        }

        Car car = carRepository.findById(informationService.getCarId()).get();

        Double priceForOneDay = car.getPrice();
        informationService.setPriceForDay(priceForOneDay);

        Double priceForAllDays = priceForOneDay * duringTimeInDays;

        if (carBorrowing.getCityEnter() != carBorrowing.getCityExit()) {
            priceForAllDays += OTHER_CITY_EXTRA_PAYMENT;
        }

        informationService.setPriceForAllDay(priceForAllDays);

        model.addAttribute("car", car.getModel());
        model.addAttribute("enter", informationService.getEnterDate());
        model.addAttribute("exit", informationService.getExitDate());
        model.addAttribute("price", informationService.getPriceForAllDay());
        model.addAttribute("cityEnter", informationService.getCityEnter());
        model.addAttribute("cityExit", informationService.getCityExit());

        return "client/travel";

    }

    @GetMapping(value = "/confirmBorrowCar")
    public String confirmBorrowCar(@ModelAttribute CarBorrowing carBorrowing) {

        String login = LoggedInUserService.getLogin();
        User user = userRepository.findUserByLogin(login);
        Car car = carRepository.findById(informationService.getCarId()).get();

        UserCar userCar = new UserCar(user, car);
        userCarRepository.save(userCar);

        car.setState(State.BORROW);
        carRepository.save(car);

        carBorrowing.setUserCar(userCar);
        carBorrowing.setDateEnter(informationService.getEnterDate());
        carBorrowing.setDateExit(informationService.getExitDate());
        carBorrowing.setPriceForDay(informationService.getPriceForDay());
        carBorrowing.setPriceForAllDay(informationService.getPriceForAllDay());
        carBorrowing.setCityEnter(informationService.getCityEnter());
        carBorrowing.setCityExit(informationService.getCityExit());

        carBorrowingRepository.save(carBorrowing);

        walletService.addMoney(informationService.getPriceForAllDay(),carBorrowing);

        return "client/confirmborrowcar";
    }

}
