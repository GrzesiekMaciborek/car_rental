package com.carrental.carrental.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Setter
@Getter
public class Car {

    @Id
    @GeneratedValue
    private Long idCar;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private Models model;

    @Column(nullable = false)
    private Double price;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private Color color;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private State state;

    private String urlPhoto;

    public Car() {
    }

    public Car(Models model, Double price, Color color, State state, String urlPhoto) {
        this.model = model;
        this.price = price;
        this.color = color;
        this.state = state;
        this.urlPhoto = urlPhoto;
    }
}
