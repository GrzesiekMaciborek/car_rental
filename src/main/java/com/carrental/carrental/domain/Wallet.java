package com.carrental.carrental.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;


@Entity
@Getter
@Setter
public class Wallet {

    @Id
    @GeneratedValue
    private Long idWallet;

    private Double cashInWallet;

    private LocalDateTime transactionDate;

    @OneToOne
    private CarBorrowing carBorrowing;

    @OneToOne
    private CarRepairing carRepairing;

    public Wallet() {
    }

    public Double addMoney(Double money) {
        return cashInWallet += money;
    }

}
