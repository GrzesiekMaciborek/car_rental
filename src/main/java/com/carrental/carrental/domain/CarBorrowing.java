package com.carrental.carrental.domain;

import lombok.Getter;
import lombok.Setter;
import org.apache.tomcat.jni.Local;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;

@Entity
@Getter
@Setter
public class CarBorrowing {

    @Id
    @GeneratedValue
    private Long idBorrowCar;

    @OneToOne
    private UserCar userCar;

    @Column(nullable = false)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dateEnter;

    @Column(nullable = false)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dateExit;

    @Column(nullable = false)
    private Double priceForDay;

    @Column(nullable = false)
    private Double priceForAllDay;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private City cityEnter;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private City cityExit;

    public CarBorrowing(UserCar userCar, LocalDate dateEnter, LocalDate dateExit, Double priceForDay, Double priceForAllDay, City cityEnter, City cityExit) {
        this.userCar = userCar;
        this.dateEnter = dateEnter;
        this.dateExit = dateExit;
        this.priceForDay = priceForDay;
        this.priceForAllDay = priceForAllDay;
        this.cityEnter = cityEnter;
        this.cityExit = cityExit;
    }

    public CarBorrowing() {
    }
}

