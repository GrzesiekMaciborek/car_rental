package com.carrental.carrental.commands;

import com.carrental.carrental.domain.*;
import com.carrental.carrental.persistance.CarRepository;
import com.carrental.carrental.persistance.UserRepository;
import com.carrental.carrental.persistance.WalletRepository;
import com.carrental.carrental.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

@ShellComponent
public class ShellCommands {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CarRepository carRepository;

    @Autowired
    private WalletRepository walletRepository;

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    @ShellMethod("Add default data for test only")
    public String add() {
        userRepository.save(new User("admin", bCryptPasswordEncoder.encode("123"), "ROLE_ADMIN", "Temp Admin", "Kowalski"));
        userRepository.save(new User("user", bCryptPasswordEncoder.encode("123"), "ROLE_USER", "Temp user", "Nowak"));
        userRepository.save(new User("tom", bCryptPasswordEncoder.encode("123"), "ROLE_USER", "Tom", "Jonnes"));
        userRepository.save(new User("kate", bCryptPasswordEncoder.encode("123"), "ROLE_USER", "Kate", "Willson"));

        List<Car> carsForTests = new ArrayList<>();

        carsForTests.add(new Car(Models.FIAT, 50.0, Color.WHITE, State.FREE, "https://images.netdirector.co.uk/gforces-auto/image/upload/w_1552,h_873,q_auto,c_fill,f_auto,fl_lossy/auto-client/95ce615aecbab21f82898d0da93bcf16/lounge_uk.png"));
        carsForTests.add(new Car(Models.TOYOTA, 80.0, Color.SILVER, State.BROKEN, "http://img.sm360.ca/ir/w500c/images/newcar/2017/toyota/corolla/ce/sedan/exteriorColors/2017_toyota_corolla_berline_ce_argent-classique-metal_001.png"));
        carsForTests.add(new Car(Models.TOYOTA, 90.0, Color.SILVER, State.FREE, "http://img.sm360.ca/ir/w500c/images/newcar/2017/toyota/corolla/ce/sedan/exteriorColors/2017_toyota_corolla_berline_ce_argent-classique-metal_001.png"));
        carsForTests.add(new Car(Models.TOYOTA, 100.0, Color.SILVER, State.BORROW, "http://img.sm360.ca/ir/w500c/images/newcar/2017/toyota/corolla/ce/sedan/exteriorColors/2017_toyota_corolla_berline_ce_argent-classique-metal_001.png"));
        carsForTests.add(new Car(Models.VOLVO, 110.0, Color.BLACK, State.FREE, "https://carwow-uk-wp-3.imgix.net/XC60-Black-Stone.jpeg?ixlib=rb-1.1.0&fit=crop&w=1600&h=&q=60&cs=tinysrgb&auto=format"));
        carsForTests.add(new Car(Models.VOLVO, 120.0, Color.RED, State.FREE, "https://carwow-uk-wp-3.imgix.net/XC60-Black-Stone.jpeg?ixlib=rb-1.1.0&fit=crop&w=1600&h=&q=60&cs=tinysrgb&auto=format"));
        carsForTests.add(new Car(Models.VOLVO, 130.0, Color.BLACK, State.FREE, "https://carwow-uk-wp-3.imgix.net/XC60-Black-Stone.jpeg?ixlib=rb-1.1.0&fit=crop&w=1600&h=&q=60&cs=tinysrgb&auto=format"));
        carsForTests.add(new Car(Models.AUDI, 140.0, Color.WHITE, State.BORROW, "http://img1.sm360.ca/ir/w600h450/images/newcar/2018/audi/a3-sportback-e-tron/progressiv/hybrid/exteriorColors/11748_cc0640_001_t9.png"));
        carsForTests.add(new Car(Models.AUDI, 150.0, Color.RED, State.FREE, "http://img1.sm360.ca/ir/w600h450/images/newcar/2018/audi/a3-sportback-e-tron/progressiv/hybrid/exteriorColors/11748_cc0640_001_t9.png"));
        carsForTests.add(new Car(Models.AUDI, 200.0, Color.WHITE, State.FREE, "http://img1.sm360.ca/ir/w600h450/images/newcar/2018/audi/a3-sportback-e-tron/progressiv/hybrid/exteriorColors/11748_cc0640_001_t9.png"));
        carsForTests.add(new Car(Models.SYRENKA, 15.0, Color.GOLD, State.FREE, "https://b.allegroimg.com/s400/03864e/b81c28144488879ce7bde18edc4b"));
        carsForTests.add(new Car(Models.SYRENKA, 150.0, Color.BLACK, State.FREE, "https://galeria.bankier.pl/p/2/9/dda7f3e6b44084-645-322-0-408-3081-1540.jpg"));
        carsForTests.add(new Car(Models.SYRENKA, 200.0, Color.GOLD, State.FREE, "https://b.allegroimg.com/s400/03864e/b81c28144488879ce7bde18edc4b"));

        carRepository.saveAll(carsForTests);

        Wallet wallet = new Wallet();
        wallet.setCashInWallet(800.0); //cash in start
        wallet.setTransactionDate(LocalDateTime.of(2007,02,02,02,02,20)); //casxh in start

        walletRepository.save(wallet);

        Wallet wallet1 = new Wallet();
        wallet1.setCashInWallet(1800.0); //cash in start
        wallet1.setTransactionDate(LocalDateTime.now()); //casxh in start

        walletRepository.save(wallet1);

        Wallet wallet2 = new Wallet();
        wallet2.setCashInWallet(0.0); //cash in start
        wallet2.setTransactionDate(LocalDateTime.of(2000,01,01,10,10,10)); //casxh in start

        walletRepository.save(wallet2);

        return String.format("Test data were added: users, cars, wallet.");
    }

    @ShellMethod("Show all Users, Cars, Wallet")
    public String show() {
        List<User> users = userRepository.findAll();

        StringBuilder result = new StringBuilder();
        int i = 0;
        result.append("List of Users:\n");
        for (User user : users) {
            result.append(++i + ") " + user.getLogin() + "\n");
        }

        List<Car> cars = carRepository.findAll();
        i = 0;
        result.append("List of Cars:\n");
        for (Car car : cars) {
            result.append(++i + ") " + car.getModel() + "\n");
        }

        //System.out.println("Wallet = " + walletRepository.findAll().toString());

        return result.toString();
    }
}